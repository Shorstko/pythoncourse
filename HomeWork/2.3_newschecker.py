import codecs
import json

with codecs.open('newsit.json', 'r', encoding="cp1251") as news_file:
    rss = json.load(news_file)['rss']
    words = {}
    for item in rss['channel']['item']:
        for word in item['description'].split():
            if word in words:
                words[word] += 1
            else:
                words[word] = 1

items = list(words.items())
print(sorted(items, key=lambda x: x[1], reverse=True))
