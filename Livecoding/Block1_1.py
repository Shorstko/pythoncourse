residence_limit = 90  # 45, 60
schengen_constraint = 180

# Мы тратим в день
cost_per_day = 15

first_of_january = 1  # первое января текущего года

first_time_arriving = 1
first_time_leave = 11

second_time_arriving = 16
second_time_leave = 27

third_time_arriving = 45
third_time_leave = 46

total_time_in_es = (first_time_leave - first_time_arriving)
total_time_in_es = total_time_in_es + (second_time_leave - second_time_arriving)
total_time_in_es = total_time_in_es + (third_time_leave - third_time_arriving)

if total_time_in_es > residence_limit:
    print('Вы не можете прибывать в ЕС так долго')

print('Вы пробудете в ЕС дней:', total_time_in_es)
print('Стоимость проживания для вас:', total_time_in_es * cost_per_day, 'евро')

# добавить перевод в рубли по текущему курс (текущий курс взять из банка)
# добавить цену на питание в день
# проверить не выходи ли мы за бюджет (у нас есть N рублей, хватит ли нам их на поездку?)

olya_budget = sum([300, 500, 12, 50, 12])

oleg_salary = [12, 300, 50, 45]
oleg_average_salary = sum(oleg_salary) / len(oleg_salary)
oleg_budget = oleg_average_salary * 5

budget = olya_budget + oleg_budget
personal_expenditures = 500
common_budget = (olya_budget + oleg_budget) - personal_expenditures * 2
common_budget_for_person_per_day = common_budget / total_time_in_es / 2
print('Персональный бюджет на день:', common_budget_for_person_per_day, 'евро')
if cost_per_day > common_budget_for_person_per_day:
    print('Похоже, нам не хватает денег на жизнь')

# построить бюджет, зная зп за несколько месяцев
#