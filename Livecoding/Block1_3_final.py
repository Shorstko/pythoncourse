residence_limit = 90  # 45, 60
schengen_constraint = 180
visits = [[1, 10], [61, 90], [101, 140], [141, 160], [271, 290]]
days_for_visits = []

for visit in visits:
    days_for_visit = 0
    for past_visit in visits:
        if visit[0] - schengen_constraint < past_visit[0] < visit[0]:
            days_for_visit += past_visit[1] - past_visit[0] + 1
    days_for_visit += visit[1] - visit[0] + 1
    days_for_visits.append(days_for_visit)

print (days_for_visits)
assert (days_for_visits == [10, 10 + 30, 10 + 30 + 40, 10 + 30 + 40 + 20, 40 + 20 + 20])

for visit, total_days in zip(visits, days_for_visits):
    if total_days > residence_limit:
        overstay_time = total_days - residence_limit
        print('Во время визита', visit, 'количество время пребывания превышено на', overstay_time, 'дней')


            # ДЗ:
            # 0.
            # В качестве подсказки можно использовать описание работы шенгенского калькулятора
            # http://www.travel.ru/formalities/visa/schengen/schengen_calculator.html
            # 1.
            # Что будет если в нашем списке будут накладывающиеся визиты?
            # Такой ситуации допустить нельзя!
            # Вывести ошибку, если есть пересекающиеся визиты.
            # Прекратить выполнение программы, команда exit()
            # 2.
            # Мы не учитываем ситуацию:
            # когда дата въезда не попадает интервал 180
            # а дата выезда - попадает
            # Например [[1, 170], [182, 220]]
            # В этом случае мы должны посмотреть,
            # сколько дней от первого заезда попадают в интервал 180 дней и вывести ошибку
            # 3.
            # Я планирую ещё один визит в будущем.
            # Я знаю дату въезда.
            # Сколько времени я смогу пробыть в шенгене?
            # 4.
            # разобраться с циклом while, придумать пример использования

