# set()
# list()
# dict()
# tulpe()

# на блоке работы с ФС, можно будет
# список стран подгружать из файл

# 1
# словари
# Задача: помощник в выборе стран.
# Куда хотим поехать, а куда не хотим?
country1 = 'Thailand'
country1_sea = True
country1_schengen = False

# c = { }
# c = {'name': 'Thailand'}
# c['sea'] = True
# print(c['name'])
# print(c)

country = {'name': 'Thailand',
           'sea': True,
           'schengen': False},

country2 = {'name': 'Poland',
            'sea': True,
            'schengen': True},

# Мы можем организовать список!

countries = [
    {'name': 'Great Britain', 'sea': True, 'schengen': False, 'exchange_rate': 80, 'month_avg_temperature': 15},
    {'name': 'Thailand', 'sea': True, 'schengen': False, 'exchange_rate': 1.8, 'month_avg_temperature': 30.3 },
    {'name': 'Poland', 'sea': True, 'schengen': True, 'exchange_rate': 17, 'month_avg_temperature': 3},
    {'name': 'Hungary', 'sea': False, 'schengen': True, 'exchange_rate': 0.23, 'month_avg_temperature': 8},
    {'name': 'Bolivia', 'sea': False, 'schengen': False, 'exchange_rate': 9.3, 'month_avg_temperature': 11}
]

# выведем все страны
#for country in countries:
#    print(country['name'])

# выведем все страны, у которых есть море
for country in countries:
    if country['sea']:
        print(country['name'])

# у нас есть N рублей, мы хотим знать, какой курс у страны с морем
# сколько у нас будет денег в валюте этой страны
print('2 ------')
rur_sum = 100000
for country in countries:
    if country['sea']:
        foreign_currency_sum = rur_sum / country['exchange_rate']
        print(country['name'], country['exchange_rate'], foreign_currency_sum)

# рассказать про %
print('3 ------')
rur_sum = 100000
for country in countries:
    if country['sea']:
        foreign_currency_sum = rur_sum / country['exchange_rate']
        # ДЗ: почитать про format
        # https://docs.python.org/3/tutorial/inputoutput.html
        # https://pyformat.info/
        print("%s %s  %.3f" % (country['name'], country['exchange_rate'], foreign_currency_sum))

#
print('4 ------')
rur_sum = 100000
for country in countries:
    if not country['sea'] and not country['schengen'] and country['month_avg_temperature'] > 15:
        foreign_currency_sum = rur_sum / country['exchange_rate']
        print("%s %s  %.3f" % (country['name'], country['exchange_rate'], foreign_currency_sum))

#
print('5 ------')
sea_countries = set()
schengen_countries = set()
hot_countries = set()

for country in countries:
    if country['sea']:
        sea_countries.add(country['name'])
    if country['schengen']:
        schengen_countries.add(country['name'])
    if country['month_avg_temperature'] > 25:
        hot_countries.add(country['name'])

print(sea_countries)
print(schengen_countries)
print(hot_countries)

print(sea_countries & schengen_countries)
print(schengen_countries | sea_countries)

print('6 ------')
# как мы будем искать детали страны?
sea_schengen = sea_countries & schengen_countries
# итерируемся по словарю
for sea_shengen_country in sea_schengen:
    for country in countries:
        if country['name'] == sea_shengen_country:
            print(country)
            break

print('7 ------')
# словарь вместо списка
countries = {
     'Great Britain': {'sea': True, 'schengen': False, 'exchange_rate': 80, 'month_avg_temperature': 15},
     'Thailand': {'sea': True, 'schengen': False, 'exchange_rate': 1.8, 'month_avg_temperature': 30.3 },
     'Poland': {'sea': True, 'schengen': True, 'exchange_rate': 17, 'month_avg_temperature': 3},
     'Hungary': {'sea': False, 'schengen': True, 'exchange_rate': 0.23, 'month_avg_temperature': 8},
     'Bolivia': {'sea': False, 'schengen': False, 'exchange_rate': 9.3, 'month_avg_temperature': 11}
}

for country, properties in countries.items():
    if not properties['sea'] and not properties['schengen']:
        print("%s %s  %.3f" % (country, properties['exchange_rate'], foreign_currency_sum))

print('8 ------')
# вывести данные по странам, которые для нас наиболее привлекателны
# (получены в результате пересечений множеств)
countries = {
     'Great Britain': {'sea': True, 'schengen': False, 'exchange_rate': 80, 'month_avg_temperature': 15},
     'Thailand': {'sea': True, 'schengen': False, 'exchange_rate': 1.8, 'month_avg_temperature': 30.3 },
     'Poland': {'sea': True, 'schengen': True, 'exchange_rate': 17, 'month_avg_temperature': 3},
     'Hungary': {'sea': False, 'schengen': True, 'exchange_rate': 0.23, 'month_avg_temperature': 8},
     'Bolivia': {'sea': False, 'schengen': False, 'exchange_rate': 9.3, 'month_avg_temperature': 11}
}

for sea_shengen_country in sea_schengen:
    print(sea_shengen_country, countries[sea_shengen_country])

print('9 ------')
# оператор in
# пришла бабушка и спросила, а Франция подходит?
appropriate_countries = schengen_countries | sea_countries
print('Poland' in appropriate_countries)

print('10 ------')
# сортировка
# print(appropriate_countries) - не сортированы
# подали на вход множество, на выходе - список
# в множества хранятся неупорядоченные значения
# также как и в словарях
print(sorted(appropriate_countries))

# ДЗ, добавить свойство: сколько в день требуется
# в день на проживание в этой стране в валюте этой страны
# добавить ещё 5 стран
# добавить множество
# прочитать http://pep8.ru/doc/tutorial-3.1/5.html
#