residence_limit = 90  # 45, 60
schengen_constraint = 180
visits = [[1, 10], [61, 90], [101, 140], [141, 160], [271, 290]]
days_for_visits = []

for visit in visits:
    days_for_visit = 0
    for past_visit in visits:
        if visit[0] - schengen_constraint < past_visit[0] < visit[0]:
            days_for_visit += past_visit[1] - past_visit[0] + 1
    days_for_visit += visit[1] - visit[0] + 1
    days_for_visits.append(days_for_visit)

print (days_for_visits)
assert (days_for_visits == [10, 10 + 30, 10 + 30 + 40, 10 + 30 + 40 + 20, 40 + 20 + 20])

# Добавление логики, на сколько дней я могу пробыть в шенгене если въеду такого-то числа

for visit, total_days in zip(visits, days_for_visits):
    if __name__ == '__main__':
        if total_days > residence_limit:
            overstay_time = total_days - residence_limit
            print('Во время визита', visit, 'количество время пребывания превышено на', overstay_time, 'дней')

next_visit = 310
visits_with_next = visits + [[next_visit, next_visit]]
days_for_visits = []
days_for_next_visit = 0

for visit in visits_with_next:
    days_for_visit = 0
    for past_visit in visits_with_next:
        if visit[0] - schengen_constraint < past_visit[0] < visit[0]:
            days_for_visit += past_visit[1] - past_visit[0] + 1
    days_for_visit += visit[1] - visit[0] + 1
    days_for_visits.append(days_for_visit)

days_for_next_visit = residence_limit - days_for_visits[len(days_for_visits) - 1] + 1

print(days_for_next_visit)
assert(days_for_next_visit == 90 - 20 - 20)

# Добавление шага с выделением самой частой операции в функцию
residence_limit = 90  # 45, 60
schengen_constraint = 180

def date_difference(last, first):
    return last - first + 1

# Выделение кода со списком дней
def get_days_for_visits(visits):
    days_for_visits = []
    for visit in visits:
        days_for_visit = 0
        for past_visit in visits:
            if visit[0] - schengen_constraint < past_visit[0] < visit[0]:
                days_for_visit += date_difference(past_visit[1], past_visit[0])
        days_for_visit += date_difference(visit[1], visit[0])
        days_for_visits.append(days_for_visit)
    return days_for_visits

visits = [[1, 10], [61, 90], [101, 140], [141, 160], [271, 290]]
days_for_visits = get_days_for_visits(visits)

print (days_for_visits)
assert (days_for_visits == [10, 10 + 30, 10 + 30 + 40, 10 + 30 + 40 + 20, 40 + 20 + 20])

visits_with_next = visits + [[next_visit, next_visit]]
days_for_visits = get_days_for_visits(visits_with_next)
days_for_next_visit = residence_limit - days_for_visits[len(days_for_visits) - 1] + 1

print(days_for_next_visit)
assert(days_for_next_visit == 90 - 20 - 20)

for visit, total_days in zip(visits, days_for_visits):
    if __name__ == '__main__':
        if total_days > residence_limit:
            overstay_time = total_days - residence_limit
            print('Во время визита', visit, 'количество время пребывания превышено на', overstay_time, 'дней')

# мы выделили общий код в отдельную функцию
# теперь попробуем написать функцию,
# которой мы бы в действительности хотели воспользоваться
# что делает моя программа?
# def print_problematic_visits(visits)

print('------')
def print_problematic_visits(visits):
    days_for_visits = get_days_for_visits(visits_with_next)

    for visit, total_days in zip(visits, days_for_visits):
        if __name__ == '__main__':
            if total_days > residence_limit:
                overstay_time = total_days - residence_limit
                print('Во время визита', visit, 'количество время пребывания превышено на', overstay_time, 'дней')


def print_available_days_for_next_date(visits, next_visit):
    visits_with_next = visits + [[next_visit, next_visit]]
    days_for_visits = get_days_for_visits(visits_with_next)
    days_for_next_visit = residence_limit - days_for_visits[len(days_for_visits) - 1] + 1

    print('Вы сможете пробыть %s дней, если въедете на %s день' % (days_for_next_visit, next_visit))


visits = [[1, 10], [61, 90], [101, 140], [141, 160], [271, 290]]
print_problematic_visits(visits)
print_available_days_for_next_date(visits, 310)

