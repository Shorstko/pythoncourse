# Блок 2. Работа с файлами

## Открытие и чтение файла, запись в файл
### Доп:
 - спросить, все ли освоились с git
 - рассказать про github
 - рссказать, что вообще хорошим тоном является отделение ввода и вывода программы 
 декомпозиция по операциям, 
 рассказать про способ организации программы в виде pipeline'а даннных
 - рассказать, что буду обращать внимание на декомпозицию, длина функции - 10 строчек
 - рассказать, что нет ничего хуже, чем закомментирвоанный код
 - показать, что можно было бы на домашке сделать словарь с функциями
 
### Знает:
 - знает что такое файл
 - режимы открытия файла
    - w, a, r, b
 - правила работы с git'ом
 - невидимые симолы (перенос строки)
 - работа со строками trim()
 
### Умеет:
 - запускать скрипт на python в консоли
 - закоммитить код на github
 - открыть текстовый файл
 - использовать конструкцию with
 
### Практика:
#### В классе:
- прочитать файл, вывести содержимое
- отфильтровать неинтересные данные из файла
- отсортировать содержимое файла
- записать результат в файл

#### Домашка:
- Чтение визитов - из файла
- Чтение информации о странах из файла
- Для всех файлов в папке

## Работа с разными форматами

### Структура
- Описание формата
- Где используется, для чего
- как читать и писать файлы этого формата

### Форматы 

- json
 - https://docs.python.org/3/library/json.html
 - import json
 - json.load(data_file)
- csv
 - https://docs.python.org/3/library/csv.html
 - попробовать разобрать настоящие данные https://www.worlddata.info/downloads/
 - или вот эти https://support.spatialkey.com/spatialkey-sample-csv-data/
 - формат: заголовок, разделители, диалект
 - связь с excel
- xml
 - https://docs.python.org/3/library/xml.etree.elementtree.html
 - import xml.etree.ElementTree as ET
 - ET.parse('country_data.xml')
 - tree.getroot()
- yaml
 - http://pyyaml.org/wiki/PyYAMLDocumentation
 - pip install pyyaml
 - Кто использует? Travis, и всякие питонисты (вроде нас) для конфигов

### Знает
 - основные форматы данных
 - их отличия
 - где они используются
 - где найти информацию про них

### Умеет
 - открыть json/xml
 - сохранить данные в json/xml/yaml

### Домашка
- Взять программу, которую писали в качестве 4ой домашки (страны)
- Залить на github
- хранить список стран в yaml или json
 - эксперименты с csv и json - по желанию

## Работа с кодировками, русскими буквами

### Темы
- Таблица символов, кодировки, для чего они нужны
- Кракозябры
- Как определить кодировку текста вручную?
- Существует ли способ определить кодировку текста автоматически?
 - Да, существуют разные эвристики
 - Но 100% гарантии ни один автоматический метод не даёт
 - В UTF бывают используются т.н. BOM байты: первые два байта, которые однозначно указывают, что файл в кодировке UTF
- Работа с кодировками в различных редакторах (sublime, pycharm)
- Строки в Python в каком формате?
 - Unicode
 - chr()
 - ord()
 
- Чтение файлов в разных кодировках в Python
- Наш код - тоже текст, его кодировка имеет значение
 - как сказать Python'у, в какой кодировке файл

```python
# -*- coding: UTF-8 -*-
```

- https://docs.python.org/3/howto/unicode.html
- https://docs.python.org/2.5/lib/standard-encodings.html


### Знает
- Что такое кодировка
- Какие кодировки бывают
- Какие проблемы бывают при работе с кодирвоками

### Умеет 
- открывать файлы в разных кодировках
- справляться с проблемами, которые возникают

## Работа с путями

### Темы
 - Сложности конкатенации путей и кроссплатформенность (разные разделители)
 - где находится выполняемый скрипт?
 - относительные и абсолютные пути
  - ../ 
  - ./
 - рабочая директория и папка в которой расположен скрипт
  
  
- __file__
- sys.argv[0]
- os.path.walk
- glob.glob
- os.path
- os.path.join
- os.path.dirname
- os.path.abspath
- os.walk
    - https://www.tutorialspoint.com/python/os_walk.htm
- проверки isdir, isfile
- os.getcwd()
- os.chdir()

### Знает 
 - отличия путей в linux и windows
  - / и \
 -  знает что такое симлинки и хардлинки
 - плюсы и минусы хранения данных рядом с кодом
  + данные рядом с кодом, могут распространяться совместно
  + можно не думать о том, как запускается программа
  + удобно, когда нет контроля над рабочей директорией при запуске программы
  + удобно, когда программой пользуются люди, которые не знают что такое рабочая директория
  - код не гибок, чтобы запустить на других данных или с другой конфигурацией, надо подменять реальные файлы
  - могут быть чувствительные данные, их стрёмно распространять рядом с кодом и придётся подкладывать после деплоймента

### Умеет
 - узнать рабочую директорию
 - открыть файл в рабочей директории
 - изменить рабочую директорию
 - найти папку а которой находится скрипт
 - открыть файл в папке со скриптом
 - найти все файлы с определённым расширением в дереве папок
 - конкатенировать части пути
 
## Вызов внешних программ

- Зачем мы этому учим?
- Todo: подумать в сторону автоматического тестирования
- - процесс с pull реквестами
- Добавить задания на зачёт "для продвинутых"
- спросить явно про формат
- - Может быть больше текста / референсов? 

 - прототип функции
 - вход
 - выход

- Работа с файлами
 - какие файлы бывают
  - текстовые
  - бинарные
  - в конечном счёте это одно и тоже, просто по-разному интерпретируем
 - создание
  - как писать в файл
  - как дописывать в конец
 - открытие
   - w, a, r, b
   - как прочитать целиком
   - как построчно
 - закрытие
- Кодировки
  - Не пугались, когда увидят, понять какие бывают ошибки с Юникодом
   
- Файлы разных форматов:
  - csv
    - https://www.worlddata.info/downloads/
  - xml
  - yml
  - json
  - pickle
  
- Работа с папками и путями
 - что такое файловая система
 - path.join()
 - os.path.*
 - текущая скрипта / папка в которой скрипт лежит - не одно и тоже
 - переменная __file__
 - os.walk
 - относительные и абсолютные пути
 - это понимание важно для работы с файлами
 

- Декомпозиция до функций
    - Постоянно использовать их: 
    - чужие из других модулей, новые свои, старые свои
-

- Задания
  - парсинг логов
  - обработка данных из csv
  - возможно, выхов внешних программ
  - ужать 100 фотографий до определённого размера
  
  