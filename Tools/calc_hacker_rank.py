import requests


def get_score(user):
    user_rank_url = "https://www.hackerrank.com/rest/hackers/%s/scores_elo" % user

    result = requests.get(user_rank_url)
    user_score_data = result.json()

    python_user_data = [s for s in user_score_data if s['name'] == 'Python'].pop()
    user_score = python_user_data['practice']['score']
    return user_score


def get_username(user_url):
    return user_url.split('/')[-1]


def get_followers(my_name):
    users = []
    user_rank_url = "https://www.hackerrank.com/rest/hackers/%s/followers?limit=100" % my_name
    result = requests.get(user_rank_url)
    my_followers = result.json()
    for follower in my_followers["models"]:
        users.append(follower.get("username"))
    return users


users = get_followers('blohin_o_d')
print(users)

result = []
for user in users:
    user_name = get_username(user)
    score = get_score(user_name)
    result.append((user_name, score))

result.sort(key=lambda x: x[1], reverse=True)

for user_name, score in result:
    print("%s: %s Points" % (user_name, score))
