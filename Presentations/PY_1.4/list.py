# Создать список
friends = []

# Заполненный список 
classmates = ['Vasya', 'Petya', 'Fyodor']

# Добавить значение в список
friends.append('Vasya')
friends.append('Kolya')

# Получить первый элемент списка
first_friend = friends[0]

# Проверить присутствие значения в списке
'Kesha' in classmates == False

# Сложить списки
friends_than_classmates = friends + classmates

# Удалить элемент списка по индексу
del friends[0]

# Обойти все элементы списка в цикле
for mate in classmates:
	print(mate)